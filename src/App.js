import React from 'react';
import './App.css';
import LazyList from './LazyList';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Lazy Loading </h1>
      </header>
      <main>
        <LazyList />
      </main>
    </div>
  );
}

export default App;