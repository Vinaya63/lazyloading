import React, { useState, useEffect, useRef, useCallback } from "react";

const LazyList = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [hasMoreItems, setHasMoreItems] = useState(true);
  const [page, setPage] = useState(1);
  const [totalItems, setTotalItems] = useState(0);
  const observer = useRef(null);

  const fetchItems = useCallback(async (pageNum) => {
    setLoading(true);
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/photos?_page=${pageNum}&_limit=10`
    );
    const data = await response.json();
    setLoading(false);
    const newItems = data.map((item) => ({
      id: item.id,
      title: item.title,
      thumbnailUrl: `https://picsum.photos/id/${item.id}/200`,
    }));
    setTotalItems((prevTotal) => prevTotal + newItems.length);
    if (totalItems >= 1000) {
      setHasMoreItems(false);
      return [];
    }
    if (newItems.length === 0) {
      setHasMoreItems(false);
    }
    return newItems;
  }, [totalItems]);

  const loadMoreItems = useCallback(async () => {
    setLoading(true);
    const newItems = await fetchItems(page + 1);
    if (newItems.length > 0) {
      setPage((prevPage) => prevPage + 1);
      setItems((prevItems) => [...prevItems, ...newItems]);
    } else {
      setHasMoreItems(false);
    }
    setLoading(false);
  }, [fetchItems, page]);

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: "0px",
      threshold: 1.0,
    };

    observer.current = new IntersectionObserver((entries) => {
      const target = entries[0];
      if (target.isIntersecting && hasMoreItems && !loading) {
        loadMoreItems();
      }
    }, options);

    if (observer.current && observer.current.disconnect) {
      observer.current.disconnect();
    }

    const loader = document.querySelector("#lazy-list-loader");
    if (loader) {
      observer.current.observe(loader);
    }

    return () => {
      if (observer.current && observer.current.disconnect) {
        observer.current.disconnect();
      }
    };
  }, [hasMoreItems, loadMoreItems, loading]);

  const containerStyle = {
    minHeight: "500px",
    position: "relative",
  };

  const loaderStyle = {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "80px",
  };

  return (
    <div style={containerStyle}>
      {items.length > 0 && (
        <ul style={{ margin: 0, padding: 0 }}>
          {items.map((item) => (
            <li key={item.id} style={{ listStyle: "none" }}>
              <img src={item.thumbnailUrl} alt={item.title} />
              <span>{item.title}</span>
            </li>
          ))}
        </ul>
      )}
      {items.length === 0 && (
        <div id="lazy-list-loader" style={loaderStyle}>
          {loading && <div>Loading...</div>}
          {!loading && !hasMoreItems && "End of List"}
        </div>
      )}
      {items.length > 0 && (
        <div
          id="lazy-list-loader"
          style={{ ...loaderStyle, marginTop: "20px" }}
        >
          {loading && <div>Loading...</div>}
          {!loading && hasMoreItems && "Loading More..."}
          {!loading && !hasMoreItems && "End of List"}
        </div>
      )}
    </div>
  );
};

export default LazyList;